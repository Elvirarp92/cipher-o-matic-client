import React, { Component } from 'react'

import InputForm from './InputForm/InputForm'

import Alert from 'react-bootstrap/Alert'

import './App.css'

class App extends Component {
  constructor() {
    super()
    this.state = {
      message: '',
      error: '',
    }
  }

  renderCipherText = (output) => {
    const stateCopy = { ...this.State }
    stateCopy.message = output.message
    stateCopy.error = output.error

    this.setState(stateCopy)
  }

  render() {
    return (
      <main>
        <h1>Cipher-O-Matic</h1>
        <p>Encrypt and decrypt text with classic algorithms!</p>
        {this.state.error && (
          <Alert variant='warning' className='alert'>
            {this.state.error}
          </Alert>
        )}
        <InputForm renderCipherText={this.renderCipherText} />
        <p className='output'>{this.state.message}</p>
        <small>
          Source code: <a href='https://gitlab.com/Elvirarp92/cipher-o-matic-client'>frontend</a>{' '}
          and <a href='https://gitlab.com/Elvirarp92/cipher-o-matic-server'>backend</a>. Released
          under <a href='https://choosealicense.com/licenses/mit/'>MIT license</a>. Made with love
          and caffeine by <a href='https://www.linkedin.com/in/elvira-ramirez/'>Ira Ramírez</a>.
        </small>
      </main>
    )
  }
}

export default App
